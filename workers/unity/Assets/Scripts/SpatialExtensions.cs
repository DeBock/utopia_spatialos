namespace Assets
{
    using Improbable.Math;
    using Improbable.Player;
    using UnityEngine;

    public static class SpatialExtensions
    {
        /* To and from Coordinates */

        public static Vector3 ToVector3(this Coordinates coordinates)
        {
            return new Vector3((float)coordinates.X, (float)coordinates.Y, (float)coordinates.Z);
        }

        public static Coordinates ToCoordinates(this Vector3 vector)
        {
            return new Coordinates(vector.x, vector.y, vector.z);
        }

        /* To and from Vector3f */

        public static Vector3f ToVector3f(this Vector3 vector)
        {
            return new Vector3f(vector.x, vector.y, vector.z);
        }

        public static Vector3 ToVector3(this Vector3f vector)
        {
            return new Vector3(vector.X, vector.Y, vector.Z);
        }

        /* To and from QuaternionData */

        public static Quaternion ToQuaternion(this QuaternionData quaternion)
        {
            return new Quaternion(quaternion.x, quaternion.y, quaternion.z, quaternion.w);
        }

        public static QuaternionData ToQuaternionData(this Quaternion quaternion)
        {
            return new QuaternionData(quaternion.x, quaternion.y, quaternion.z, quaternion.w);
        }
    }
}