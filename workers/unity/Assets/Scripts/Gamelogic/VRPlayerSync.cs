namespace Assets.Gamelogic
{
    using Improbable.Player;
    using Improbable.Unity.Visualizer;
    using UnityEngine;

    public sealed class VRPlayerSync : MonoBehaviour
    {
        [SerializeField]
        private GameObject _local;

        [SerializeField]
        private GameObject _remote;

        [Require]
        private ClientAuthorityCheck.Reader _clientAuthority;

        private void Start()
        {
            if (_clientAuthority.HasAuthority)
            {
                Destroy(_remote);
            }
            else
            {
                Destroy(_local);
            }
        }
    }
}