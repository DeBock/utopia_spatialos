namespace Assets.Gamelogic
{
    using Improbable.Player;
    using Improbable.Unity.Visualizer;
    using UnityEngine;

    public sealed class VRPlayerSyncSender : MonoBehaviour
    {
        [SerializeField]
        private Transform _head;

        [SerializeField]
        private Transform _leftHand;

        [SerializeField]
        private Transform _rightHand;

        [Require]
        private VRPlayer.Writer _vrPlayer;

        private void Update()
        {
            var basePosition = transform.position.ToCoordinates();
            var headOffset = GetOffset(_head);
            var leftOffset = GetOffset(_leftHand);
            var rightOffset = GetOffset(_rightHand);

            _vrPlayer.Send(new VRPlayer.Update()
                .SetBasePosition(basePosition)
                .SetHead(headOffset)
                .SetLeftHand(leftOffset)
                .SetRightHand(rightOffset));
        }

        private Offset GetOffset(Transform child)
        {
            return new Offset(child.localPosition.ToVector3f(), child.rotation.ToQuaternionData());
        }
    }
}