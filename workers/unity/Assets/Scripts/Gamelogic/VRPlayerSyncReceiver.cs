namespace Assets.Gamelogic
{
    using Improbable.Player;
    using Improbable.Unity.Visualizer;
    using UnityEngine;

    public sealed class VRPlayerSyncReceiver : MonoBehaviour
    {
        [SerializeField]
        private Transform _head;

        [SerializeField]
        private Transform _leftHand;

        [SerializeField]
        private Transform _rightHand;

        [Require]
        private VRPlayer.Reader _vrPlayer;

        private void OnEnable()
        {
            _vrPlayer.ComponentUpdated += ComponentUpdated;
        }

        private void Disable()
        {
            _vrPlayer.ComponentUpdated -= ComponentUpdated;
        }

        private void ComponentUpdated(VRPlayer.Update update)
        {
            if (update.basePosition.HasValue)
            {
                this.transform.position = update.basePosition.Value.ToVector3();
            }

            if (update.leftHand.HasValue)
            {
                ApplyOffset(update.leftHand.Value, _leftHand);
            }

            if (update.rightHand.HasValue)
            {
                ApplyOffset(update.rightHand.Value, _rightHand);
            }

            if (update.head.HasValue)
            {
                ApplyOffset(update.head.Value, _head);
            }
        }

        private void ApplyOffset(Offset offset, Transform child)
        {
            child.localPosition = offset.position.ToVector3();
            child.rotation = offset.rotation.ToQuaternion();
        }
    }
}