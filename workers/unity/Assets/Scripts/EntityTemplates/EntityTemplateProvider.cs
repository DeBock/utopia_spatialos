﻿using Improbable.Math;
using Improbable.Player;
using Improbable.Unity.Core.Acls;
using Improbable.Worker;
using UnityEngine;

namespace Assets.EntityTemplates
{
    public class EntityTemplateProvider : MonoBehaviour
    {
        public static SnapshotEntity GeneratePlayerSnapshotEntityTemplate()
        {
            // Set name of Unity prefab associated with this entity
            var playerEntity = new SnapshotEntity { Prefab = "VRPlayer" };

            // Define components attached to snapshot entity
            var offset = new Offset(Vector3f.ZERO, new QuaternionData(0f, 0f, 0f, 0f));
            playerEntity.Add(new VRPlayer.Data(new VRPlayerData(Coordinates.ZERO, offset, offset, offset)));
            playerEntity.Add(new ClientAuthorityCheck.Data(new ClientAuthorityCheckData()));

            var acl = Acl.Build()
                // Both FSim (server) workers and client workers granted read access over all states
                .SetReadAccess(CommonPredicates.PhysicsOrVisual)
                // Only client workers can modify VRPlayer
                .SetWriteAccess<VRPlayer>(CommonPredicates.VisualOnly)
                // Only FSim (server) workers can modify ClientAuthorityCheck.
                .SetWriteAccess<ClientAuthorityCheck>(CommonPredicates.PhysicsOnly);

            playerEntity.SetAcl(acl);
            return playerEntity;
        }
    }
}